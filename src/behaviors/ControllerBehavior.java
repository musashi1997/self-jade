package behaviors;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

import java.util.Arrays;
import java.util.Random;

import ir.ac.sbu.Controller;
public class ControllerBehavior extends TickerBehaviour {

    private long period;
    private int i = 0;
    private int j = 0;
    private Random rand = new Random();
    int x = 17 ;
    int y = 0;
    private Controller myAgent;
    public ControllerBehavior(Controller a, long period) {
        super(a, period);
        this.period = period;
        this.myAgent = a;
    }
    
    @Override
    protected void onTick() {
    	//TODO planter function
    	String[][] board = myAgent.getTableValuesArray();
    	int tempX,tempY;
    	while(true) {
    		tempX = rand.nextInt(20);
    		tempY = rand.nextInt(20);
    		if(board[tempX][tempY].isBlank()||board[tempX][tempY] == null) {
    			myAgent.setTableValue("G", tempX, tempY);
    			break;
    		}else {
    			System.out.println("trying again colided with "+board[tempX][tempY]+"at"+tempX +" " + tempY);
    		}
    	}
    	
    }
}
