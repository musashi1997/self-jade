package behaviors;

import ir.ac.sbu.Controller;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
//import jade.domain.introspection.ACLMessage;
import jade.lang.acl.ACLMessage;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.*;
public class SeekerBehavior extends TickerBehaviour {

		private long period;
	    private int x = 0;	//starting location
	    private int y = 19;		
	    private boolean reverseTraverse= false;
	    private boolean flag = true;
	    private Agent myAgent;
	    public final static String SEEKERMESSAGE = "FOUND:";
	    public SeekerBehavior( Agent a, long period) {
	        super(a, period);
	        this.period = period;
	        this.myAgent = a;
	    }
	    @Override
	    protected void onTick() {
	    	
	    	if(Controller.getTableValues(x, y) == "G") {	//if there is a goal send message to all other carriers
	    		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
	    		msg.setContent(SEEKERMESSAGE + (String.valueOf(x)) + "," + (String.valueOf(y)));
	    		for (int i = 1; i<=3; i++) {
	    			msg.addReceiver(new AID("ca"+i, AID.ISLOCALNAME));
		    		System.out.println("ca"+i+" found at ");
	    		}
	    			
	    		myAgent.send(msg);
	    	}
	    	if((x==0 && y < 19 && flag) ||(x==19 && flag)) { //traverse in the board
	    		reverseTraverse = !reverseTraverse;
	    		y = y-1;
	    		flag = false;
	    	} else {
	    		flag = true;
	    		if(reverseTraverse) {
	        		x = x-1;
	        	}else {
	        		x = x+1;
	        	}
	    	}
	    }
}
