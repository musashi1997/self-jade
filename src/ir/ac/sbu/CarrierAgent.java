package ir.ac.sbu;

import jade.core.Agent;

import java.util.LinkedList;
import java.util.Queue;

import jade.core.AID;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.*;

public class CarrierAgent extends Agent {
	private static boolean IAmTheCreator = true;
	// number of answer messages received.
	private int period = 1000 * 1; // 1 sec

	public final static String SEEKERMESSAGE = "FOUND:";
	public final static String SETREGION = "SETREGION:";
	public final static String FOUNDG = "FOUNDG:";
	public final static String PICKEDUP = "PICKEDUP:";
	public final static String DROPPED = "DROPPED";
	public final static String MOVING = "MOVING:";
	public final static String FROM = "FROM:";
	public final static String THANKS = "THANKS";
	private boolean reverseTraverse = false;
	private boolean flag = true;
	private boolean isCarrying = false;
	private int boundryX1 = 0;
	private int boundryX2 = 6;
	private int destX = -1;
	private int destY = 0;
	private int memoryX;
	private int memoryY;
	private int returnX;
	private int returnY;

	private int x;
	private int y;

	Queue<int[]> queue = new LinkedList<int[]>();

	public void sendPickedUp() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent(PICKEDUP + (String.valueOf(x)) + "," + (String.valueOf(y)));
		msg.addReceiver(new AID("controller", AID.ISLOCALNAME));
		send(msg);
	}

	public void sendMovement() { // send
		ACLMessage msg1 = new ACLMessage(ACLMessage.INFORM);
		msg1.setContent(FROM + (String.valueOf(memoryX)) + "," + (String.valueOf(memoryY)));
		msg1.addReceiver(new AID("controller", AID.ISLOCALNAME));
		send(msg1);
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent(MOVING + (String.valueOf(x)) + "," + (String.valueOf(y)));
		msg.addReceiver(new AID("controller", AID.ISLOCALNAME));
		send(msg);
	}

	@Override
	protected void setup() {

		if (getLocalName().contains("ca1")) {
			x = 0;
		} else if (getLocalName().contains("ca2")) {

			x = 7;
			boundryX1 = 7;
			boundryX2 = 12;
		} else if (getLocalName().contains("ca3")) {
			x = 13;
			boundryX1 = 13;
			boundryX2 = 19;
		}

		addBehaviour(new TickerBehaviour(this, period) {
			@Override
			protected void onTick() {
				memoryX = x; // memory to remember last place (removing from board)
				memoryY = y;
				if (boundryX1 != -1) { // ensure boundry is set
					// move toward region if not in a designated region
					if (destX == -1) { 
						if (boundryX1 > x) {
							destX = boundryX1;
							destY = 0;
						} else {
							destX = -2;
							destY = 0;
						}
					}
					// seeking mode
					if (destX == -2 && queue.isEmpty()) { // if no destination and empty queue
						
						//traverse region between boundryX and boundryY 
						if ((x == boundryX1 && y > 0 && flag) || (x == boundryX2 && flag)) { // reached to boundry move one
//																									to right and initiate reverse traverse
//																											upwards => downwards
//																											downwards => upwards
							reverseTraverse = !reverseTraverse;
							y = y + 1;
							flag = false; // flag to ensure movement to right only occurs once per reaching the boundries 
							
							if (Controller.getTableValues(x, y) == "G") {// if found a G (food,goal .. ) in current movement head towards store at x=10 y=10 and save current loc to return 
								//												to current location after delivery and resume traverse normally after
								isCarrying = true;
								returnX = x;
								returnY = y;
								destX = 10;
								destY = 10;
								sendPickedUp();
							}
						} else if (x >= boundryX1 && x <= boundryX2) {	
							//traverse in upwards or downwards 
							flag = true;	
							if (reverseTraverse) {
								x = x - 1;
								if (Controller.getTableValues(x, y) == "G") { // same as line 114 comment
									isCarrying = true;
									returnX = x;
									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
							} else {
								x = x + 1;
								if (Controller.getTableValues(x, y) == "G") {
									isCarrying = true;
									returnX = x;
									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
							}
						} else { // rare case if was not in boundries and dest was set to destX=-2 (seeking mode) to be able to return to designated region
							if (destX == returnX && destY == returnY) {
								destX = -2;
							} else { // case G was not there anymore 
								destX = returnX;
								destY = returnY;
								isCarrying = false; 
							}
						}
						sendMovement();

					} else if (!queue.isEmpty() && destX == -2) { // if there was a destination in queue (got from seekerAgent) DEQUEEU it and set as destination
//																		AND save current location as returning point 
						returnX = x;
						returnY = y;
						destX = queue.peek()[0];
						destY = queue.peek()[1];
						queue.remove();
					} else {
						if (isCarrying) { // Moving towards destination(store) while carrying
							if (x > destX) {
								x = x - 1;// DOWN
								if (Controller.getTableValues(x, y) == "G") { //case while moving towards store and seen a G store it's location in queue
									if (!queue.contains(new int[] { x, y }))
										queue.add(new int[] { x, y });
								}
								// declare movement to controll agent via ACLmessage
								sendMovement();
							} else if (x < destX) {
								x = x + 1;// UP
								if (Controller.getTableValues(x, y) == "G") {//case while moving towards store and seen a G store it's location in queue
									if (!queue.contains(new int[] { x, y }))
										queue.add(new int[] { x, y });
								}
								sendMovement();
							} else if (x == destX && y > destY) {
								y = y - 1;// LEFT
								if (Controller.getTableValues(x, y) == "G") {//case while moving towards store and seen a G store it's location in queue
									if (!queue.contains(new int[] { x, y }))
										queue.add(new int[] { x, y });
								}
								sendMovement();
							} else if (x == destX && y < destY) {
								y = y + 1;// RIGHT
								if (Controller.getTableValues(x, y) == "G") {//case while moving towards store and seen a G store it's location in queue
									if (!queue.contains(new int[] { x, y }))
										queue.add(new int[] { x, y });
								}
								sendMovement();

							}
							if (destX == x && destY == y) { //reached to store 
								isCarrying = false;
								ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
								msg.setContent(DROPPED);
								msg.addReceiver(new AID("controller", AID.ISLOCALNAME));
								send(msg);						// send dropped message to controller agent
								if (queue.isEmpty()) {			// if queue is empty start returning to saved traverse location
									destX = returnX;
									destY = returnY;
								} else {						//else dequeue and move towards new goal
									destX = queue.peek()[0];
									destY = queue.peek()[1];
									queue.remove();
								}
							}
//							if (Controller.getTableValues(x, y) == "G") { // add goal if seen while carrying
//								queue.add(new int[] { x, y });
//							}
						} else if (destX >= 0 && !isCarrying) {//  towards dest(probable goal) mode one step at a tick
							if (x > destX) {
								// TODO declare movement to controll agent via ACLmessage
								x = x - 1;
								if (Controller.getTableValues(x, y) == "G") {
									isCarrying = true;
									if (!queue.contains(new int[] { destX, destY }))
										queue.add(new int[] { destX, destY });
//									returnX = x;
//									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
								sendMovement();
							} else if (x < destX) {
								x = x + 1;
								if (Controller.getTableValues(x, y) == "G") {
									isCarrying = true;
									if (!queue.contains(new int[] { destX, destY }))
										queue.add(new int[] { destX, destY });
//									returnX = x;
//									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
								sendMovement();
							} else if (x == destX && y > destY) {
								y = y - 1;
								if (Controller.getTableValues(x, y) == "G") {
									isCarrying = true;
									if (!queue.contains(new int[] { destX, destY }))
										queue.add(new int[] { destX, destY });
//									returnX = x;
//									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
								sendMovement();
							} else if (x == destX && y < destY) {
								y = y + 1;
								if (Controller.getTableValues(x, y) == "G") {
									isCarrying = true;
									if (!queue.contains(new int[] { destX, destY }))
										queue.add(new int[] { destX, destY });
//									returnX = x;
//									returnY = y;
									destX = 10;
									destY = 10;
									sendPickedUp();
								}
								sendMovement();
							}
							if (destX == x && destY == y) { //reaching to dest
//																
								if (Controller.getTableValues(x, y) == "G") {// picked up
									isCarrying = true;
									System.out.println("pickin up");
									sendPickedUp();
								} else if (destX == returnX && destY == returnY) {// if reached to return point  set destX=-2 
									destX = -2;
								} else { // case it was not there anymore
									destX = returnX;
									destY = returnY;
									isCarrying = false;
								}
							}
						}
					}
				}
			}
		});
		addBehaviour(new CyclicBehaviour(this) {
			public void action() {
				// listen if a greetings message arrives
				ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
				if (msg != null) {
					if (msg.getContent().toLowerCase().contains(SEEKERMESSAGE.toLowerCase())) {

						// if a greetings message is arrived then send an ANSWER
						String[] numbersPart = msg.getContent().substring(SEEKERMESSAGE.length()).split(",");
						int recvdX = Integer.parseInt(numbersPart[0]);
						int recvdY = Integer.parseInt(numbersPart[1]);
						if (recvdX >= boundryX1 && recvdX <= boundryX2) {
							if (!queue.contains(new int[] { recvdX, recvdY })) {
								queue.add(new int[] { recvdX, recvdY });
								System.out.println(getLocalName() + " roger roger");
							}
						}

					} else {
						System.out.println(myAgent.getLocalName() + " Unexpected message received from "
								+ msg.getSender().getLocalName());
					}
				} else {
					// if no message is arrived, block the behaviour
					block();
				}
			}
		});
	}

}
