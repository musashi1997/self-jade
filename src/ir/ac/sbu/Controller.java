package ir.ac.sbu;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentContainer;

import java.awt.EventQueue;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.border.MatteBorder;
import java.awt.Window.Type;
import java.util.Random;

import jade.wrapper.AgentController;
import javax.swing.JTable;
import java.awt.Canvas;
import java.awt.TextField;
import java.awt.Panel;
import java.awt.GridLayout;
import javax.swing.table.DefaultTableModel;
import behaviors.ControllerBehavior;

public class Controller extends Agent {

	private JFrame frmAgentscontroll;
	private static JTable table;
	public static TextField textField_1 = new TextField();
	public static TextField textField = new TextField();
	public static int score = 0;
	public static int time = 0;
	public final static String SEEKERMESSAGE = "FOUND:";
	public final static String SETREGION = "SETREGION:";
	public final static String FOUNDG = "FOUNDG:";
	public final static String PICKEDUP = "PICKEDUP:";
	public final static String DROPPED = "DROPPED";
	public final static String MOVING = "MOVING:";
	public final static String FROM = "FROM:";

	@Override
	protected void setup() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Controller window = new Controller();
					window.frmAgentscontroll.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		String[] name = new String[3];

		for (int i = 0; i < name.length; i++) {
			name[i] = "ca" + (i + 1);
		}
		AgentContainer c = getContainerController();
		AgentController[] a = new AgentController[3];
		for (int j = 0; j < a.length; j++) {
			try {
				a[j] = c.createNewAgent(name[j], "ir.ac.sbu.CarrierAgent", null);
				a[j].start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//create seeker agent
		String[] seeker = new String[1];
		for (int i = 0; i < seeker.length; i++) {
			seeker[i] = "seeker" + (i + 1);
		}
		AgentContainer sc = getContainerController();
		AgentController[] sa = new AgentController[1];
		for (int j = 0; j < sa.length; j++) {
			try {
				sa[j] = c.createNewAgent(seeker[j], "ir.ac.sbu.SeekerAgent", null);
				sa[j].start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		ControllerBehavior behavior = new ControllerBehavior(this, 1000 * 5); 
		addBehaviour(behavior);
		addBehaviour(new TickerBehaviour(this, 1000*1) {
			
			@Override
			protected void onTick() {
				// TODO Auto-generated method stub
				if(time <= 120)
					textField.setText("Time: " + (time++) +"s");
				
			}
		});
		addBehaviour(new CyclicBehaviour(this) {
			public void action() {
				// listen if a greetings message arrives
				ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
				if (time <= 120  && msg != null) {
					if (msg.getContent().toLowerCase().contains(MOVING.toLowerCase())) {
						// TODO implement
						String[] numbersPart = msg.getContent().substring(MOVING.length()).split(",");
						int recvdX = Integer.parseInt(numbersPart[0]);
						int recvdY = Integer.parseInt(numbersPart[1]);
						setTableValueMovement(msg.getSender().getLocalName(), recvdX, recvdY);

					} else if (msg.getContent().toLowerCase().contains(FROM.toLowerCase())) {

						String[] numbersPart = msg.getContent().substring(FROM.length()).split(",");
						int recvdX = Integer.parseInt(numbersPart[0]);
						int recvdY = Integer.parseInt(numbersPart[1]);

						if (getTableValues(recvdX, recvdY) != "store" && getTableValues(recvdX, recvdY) != "G") {
							setTableValue("", recvdX, recvdY);
						}

					} else if (msg.getContent().toLowerCase().contains(DROPPED.toLowerCase())) {
						++score;
						textField_1.setText("Score: " + score);

					} else if (msg.getContent().toLowerCase().contains(PICKEDUP.toLowerCase())) {
						String[] numbersPart = msg.getContent().substring(PICKEDUP.length()).split(",");
						int recvdX = Integer.parseInt(numbersPart[0]);
						int recvdY = Integer.parseInt(numbersPart[1]);
						setTableValue("", recvdX, recvdY);

					} else {
						System.out.println(myAgent.getLocalName() + " Unexpected message received from "
								+ msg.getSender().getLocalName());
					}
				} else {
					// if no message is arrived, block the behaviour
					block();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Controller() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAgentscontroll = new JFrame();
		frmAgentscontroll.setResizable(false);
		frmAgentscontroll.setTitle("AgentsControll");
		frmAgentscontroll.setForeground(Color.LIGHT_GRAY);
		frmAgentscontroll.setBackground(Color.WHITE);
		frmAgentscontroll.setBounds(100, 100, 1202, 720);
		frmAgentscontroll.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmAgentscontroll.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Tools");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Start");
		mnNewMenu.add(mntmNewMenuItem);

		Panel panel = new Panel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setForeground(Color.BLACK);
		frmAgentscontroll.getContentPane().add(panel, BorderLayout.NORTH);

		textField_1.setForeground(Color.WHITE);
		textField_1.setBackground(Color.DARK_GRAY);
		textField_1.setText("Score: 0");
		textField_1.setEditable(false);
		panel.add(textField_1);

		panel.add(textField);
		textField.setForeground(Color.WHITE);
		textField.setText("Time: 0s");
		textField.setBackground(Color.DARK_GRAY);
		textField.setEditable(false);

		JPanel gameBoard = new JPanel();
		frmAgentscontroll.getContentPane().add(gameBoard, BorderLayout.CENTER);
		gameBoard.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel board = new JPanel();

		gameBoard.add(board);

		table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setPreferredSize(new Dimension(700, 700));
		table.setLocation(0, 0);
		table.setRowHeight(30);
		table.setModel(new DefaultTableModel(20, 20));

		table.getModel().setValueAt("store", 10, 10);
		placeInitialGoals();
		board.add(table, BorderLayout.SOUTH);
		// setTableValue("a");
	}

	private void placeInitialGoals() {
		int tempX, tempY;
		String[][] boardInstance = this.getTableValuesArray();
		Random rand = new Random();
		int i = 0;
//		table.getModel().setValueAt("G", 4, 0);
//		table.getModel().setValueAt("G", 6, 0);
//		table.getModel().setValueAt("G", 4, 9);
//		table.getModel().setValueAt("G", 2, 19);
		while (i != 3) {
			boardInstance = this.getTableValuesArray();
			tempX = rand.nextInt(20);
			tempY = rand.nextInt(20);
			if (boardInstance[tempX][tempY].isBlank() || boardInstance[tempX][tempY] == null) {
				table.getModel().setValueAt("G", tempX, tempY);
				System.out.println("placed at:" + tempX + " " + tempY);
				i++;
			} else {
				System.out.println("colided with sth at" + tempX + " " + tempY);
			}
		}
	}

	public void setTableValue(String a, int x, int y) {
		table.getModel().setValueAt(a, x, y);
	}

	public void setTableValueMovement(String a, int x, int y) {
		if (table.getModel().getValueAt(x, y) == "store") {

		} else if (table.getModel().getValueAt(x, y) == "G") {
//			table.getModel().setValueAt(a+"[G]", x, y);
		} else {
			table.getModel().setValueAt(a, x, y);
		}
	}

	public String[][] getTableValuesArray() {
		String[][] res = new String[20][20];
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {

				res[i][j] = (table.getModel().getValueAt(i, j) == null) ? ""
						: table.getModel().getValueAt(i, j).toString();
			}
		}
		return res;
	}

	public static String getTableValues(int x, int y) {
		return (table.getModel().getValueAt(x, y) == null) ? "" : table.getModel().getValueAt(x, y).toString();
	}
}
