package ir.ac.sbu;

import java.awt.EventQueue;

import behaviors.*;
import jade.core.Agent;
import jade.core.AID;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.*;
public class SeekerAgent extends Agent {
	

	@Override
	protected void setup() {
		SeekerBehavior behavior = new SeekerBehavior(this, 1000 * 1);
		addBehaviour(behavior);
		AMSAgentDescription [] agents = null;
        
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this, new AMSAgentDescription (), c );
        }
        catch (Exception e) { 
        	System.out.println(e.getStackTrace());
        }
        AID myID = getAID();
        for (int i=0; i<agents.length;i++)
        {
            AID agentID = agents[i].getName();
            System.out.println(
                ( agentID.equals( myID ) ? "*** " : "    ")
                + i + ": " + agentID.getName() 
            );
        }
	}
}
